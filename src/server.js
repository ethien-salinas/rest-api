require('dotenv').config()
const express = require('express')
const jwt = require('jsonwebtoken')
const courses = require('./routes/courses')
const bodyParser = require('body-parser')
const { createStore } = require('./lib/db_connection')

const app = express()
const port = 3000
const store = createStore()

app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())
app.set('store', store)

app.get('/', (req, res) => res.send('Courses API'))

app.post('/login', (req, res) => {
    const {user:username, password} = req.body
    if (!(username === 'ethien' && password === 'qwerty')) {
        res.status(401).send({
            error: 'Usuario o contraseña inválida'
        })
        return
    }
    const tokenData = {
        username,
        roles: ['admin', 'copywriter']
    }
    const token = jwt.sign(tokenData, process.env.JWT_SECRET, { expiresIn: 60 * 60 * 24 })
    res.send({ token })
})

app.use('/courses', courses)

app.listen(port, () => console.log(`Example app listening on port ${port}!`))